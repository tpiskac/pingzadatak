﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PingZadatak.Engine;
using System.Threading;
using NDesk.Options;

namespace PingZadatak
{

    /// <summary>
    /// Istestirano na lokalnom računalu, točnije na localhostu i još jednoj lokalnoj adresi.
    /// </summary>

    class Program
    {
        static void Main(string[] args)
        {
            // Process command-line arguments.

            bool p = false;
            bool c = false;
            int port = -1;
            string bind = "";
            int mps = 1;
            short  size = 300;
            string hostname = "";

            var parser = new OptionSet()
            {
                { "p:", "Start as Pitcher.", v => p = true },
                { "c:", "Start as Catcher.", v => c = true },
                { "port=", "[Pitcher] TCP socket port to connect through. [Catcher] TCP socket port to listen on.", v => port = int.Parse(v) },
                { "bind=", "[Catcher] TCP socket address to listen on.", v => bind = v },
                { "mps=", "[Pitcher] Number of messages sent per second.", v => mps = int.Parse(v) },
                { "size=", "[Pitcher] Size in bytes of every message.", v => size = short.Parse(v) },
                { "hostname=", "[Pitcher] Network name of a computer to connect to.", v => hostname = v }
            };
            
            try
            {
                parser.Parse(args);
            }
            catch (OptionException ex)
            {
                Console.WriteLine("Error in command-line arguments:");
                Console.WriteLine(ex.Message);
                return;
            }

            // Osiguraj da je Pitcher/Catcher mod ispravno određen.

            if (p && c)
            {
                Console.WriteLine("Program can not be used in both Pitcher and Catcher modes.");
                return;
            }

            if (!p && !c)
            {
                Console.WriteLine("You must specify whether the program should be used as Pitcher (-p) or Catcher (-c).");
                return;
            }

            // Pitcher

            if (p)
            {
                Console.WriteLine(); // Ljepota ispisa.

                var pitcher = new Pitcher(Thread.CurrentThread)
                {
                    Hostname = hostname,
                    Mps = mps,
                    Port = port,
                    Size = size
                };

                // Kako bi se statistike mogle vaditi svake sekunde, čak i ako TcpClient zašteka.
                var task = pitcher.StartAsync();

                var sw = new Stopwatch(); // Za provjeru svake sekunde.
                sw.Start();

                while (!pitcher.IsFaulted)
                {
                    while (sw.ElapsedMilliseconds < 1000) { }
                    sw.Restart();

                    if (pitcher.IsFaulted) break;

                    var stat = pitcher.GetStatistics(true); // Statistike poslijednje sekunde.
                    Console.WriteLine(stat + "\n");
                }

                if (pitcher.IsFaulted)
                {
                    if (task != null)
                        task.Wait();

                    if (pitcher != null)
                        pitcher.AskStop();

                    Console.WriteLine("\n" + pitcher.ErrorMessage);
                }
            }

            //Catcher

            else if (c)
            {
                while (true)
                {
                    var catcher = new Catcher(Thread.CurrentThread)
                    {
                        Port = port,
                        Bind = bind
                    };

                    var task = catcher.StartAsync();

                    while (!catcher.IsFaulted && !task.IsCompleted) { }

                    if (catcher.IsFaulted)
                    {
                        if (task != null)
                            task.Wait();

                        if (catcher != null)
                            catcher.AskStop();

                        Console.WriteLine("\n" + catcher.ErrorMessage);
                    }
                }
            }
        }
    }
}
