﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingZadatak.Engine
{
    /// <summary>
    /// Abstraction of a ping message so that number, length and send time can be read easily.
    /// Does not note whole byte array.
    /// </summary>
    
    public class Message
    {
        /// <summary>
        /// Every message that is not generated from byte array or new version of an old message should have a new number.
        /// </summary>
        private static int _counter = 0;

        public int Number { get; private set; }
        public short Length { get; private set; }

        /// <summary>
        /// Notes creation of the Message object, or last time <see cref="AssumeSendNow"/> has been called.
        /// It is always set to <see cref="DateTime.UtcNow"/>
        /// </summary>
        public long SentMicrosecUtc { get; private set; }

        /// <summary>
        /// Random bytes leftover.
        /// </summary>
        public byte[] Filler { get; private set; }

        /// <summary>
        /// GET: Message as the <see cref="byte[]"/> object for sending, with length according to <see cref="Length"/>.
        /// SET: Private, but does not litterally note the <see cref="byte[]"/> object. It simply stores the information from it
        /// into Message's own properties.
        /// </summary>
        public byte[] Bytes
        {
            get
            {
                var output = new byte[Length];

                using (var stream = new MemoryStream(output, true))
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write(Number);
                    writer.Write(Length);
                    writer.Write(SentMicrosecUtc);
                    writer.Write(Filler);
                    writer.Close();
                }

                return output;
            }
            private set
            {
                using (var stream = new MemoryStream(value, false))
                using (var reader = new BinaryReader(stream))
                {
                    Number = reader.ReadInt32();
                    Length = reader.ReadInt16();

                    if (Length < 50) Length = 50;
                    if (Length > 3000) Length = 3000;

                    SentMicrosecUtc = reader.ReadInt64();
                    Filler = reader.ReadBytes(Length - 14);

                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Generate new message auto-incrementable according to inner counter.
        /// </summary>
        /// <param name="length">Length in bytes of the message, between 50 and 3000. 
        /// It is recommended to make sure core procedures work with right length values.</param>

        public Message(short length)
        {
            if (length < 50) length = 50;
            if (length > 3000) length = 3000;

            Number = _counter++;
            Length = length;
            SentMicrosecUtc = DateTime.UtcNow.Ticks / 10;

            Filler = new byte[Length - 14];
            new Random((int)DateTime.Now.Ticks).NextBytes(Filler);
        }

        /// <summary>
        /// Generate new version of an old message, preserving its <see cref="Number"/>, but changing <see cref="Filler"/>.
        /// </summary>
        /// <param name="length">Length in bytes of the message, between 50 and 3000. 
        /// It is recommended to make sure core procedures work with right length values.</param>
        /// <param name="number">Message identification number, <see cref="Number"/>.</param>

        public Message(short length, int number)
        {
            if (length < 50) length = 50;
            if (length > 3000) length = 3000;

            Number = number;
            Length = length;
            SentMicrosecUtc = DateTime.UtcNow.Ticks / 10;

            Filler = new byte[Length - 14];
            new Random((int)DateTime.Now.Ticks).NextBytes(Filler);
        }

        /// <summary>
        /// Generate message according to the input byte array, but does not literally store the array. Auto fixes <see cref="Length"/>,
        /// but it is still recommended to make sure core procedures work with right length values.
        /// </summary>
        /// <param name="input">Input byte array.</param>

        public Message(byte[] input)
        {
            Bytes = input;
        }

        /// <summary>
        /// Update <see cref="SentMicrosecUtc"/> to <see cref="DateTime.UtcNow"/>.
        /// </summary>

        public void AssumeSendNow()
        {
            SentMicrosecUtc = DateTime.UtcNow.Ticks / 10;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Human-readable string, for debugging or any other purpose.</returns>

        public string ToRepresentable()
        {
            return $"Num: { Number }\t Len: { Length }\t SentMicr: { SentMicrosecUtc }\t Filler: { Encoding.ASCII.GetString(Filler) }";
        }
    }
}
