﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Diagnostics;

namespace PingZadatak.Engine
{
    /// <summary>
    /// Responsible for ping-ing the local network using <see cref="Message.Bytes"/>.
    /// Core of the Ping application, isolated from GUI in order to be embeddable into any GUI.
    /// </summary>
    public class Pitcher
    {
        public int Port { get; set; }
        public int Mps { get; set; }
        public short Size { get; set; }
        public string Hostname { get; set; }

        /// <summary>
        /// How long to wait between messages. Number of <see cref="Stopwatch"/> ticks.
        /// </summary>
        public int PeriodSwTicks { get { return (int)(1.0 / Mps * Stopwatch.Frequency); } }

        /// <summary>
        /// How long to wait between messages. Number of <see cref="TimeSpan"/> ticks.
        /// </summary>
        public int PeriodTsTicks { get { return (int)(1.0 / Mps * TimeSpan.TicksPerSecond); } }

        /// <summary>
        /// How long to wait between messages. <see cref="TimeSpan"/> variant.
        /// </summary>
        public TimeSpan PeriodTs { get { return new TimeSpan(1 * TimeSpan.TicksPerSecond); } }

        public string ErrorMessage { get; set; }
        public bool IsFaulted { get; set; }

        public PingResult FailedResultForCurrent
        {
            get
            {
                return new PingResult
                {
                    messageNumber = _currentMessage.Number,
                    failed = true,
                    receiveMillis = 0,
                    sendMillis = 0,
                    roundMillis = 0
                };
            }
        }

        private int _messagesSentOverall;
        private Message _currentMessage;
        private TcpClient _client;
        private List<PingResult> _results;

        private Thread _mainThread;
        private bool _generatingStatistics;
        private bool _takingResult;

        private bool _stop;

        public Pitcher(Thread mainThread)
        {
            _mainThread = mainThread;
            IsFaulted = false;
        }

        /// <summary>
        /// If configuration is invalid, sets <see cref="IsFaulted"/> to true and sets <see cref="ErrorMessage"/>.
        /// </summary>
        /// <returns>If configuration is correct.</returns>

        private bool CheckConfig()
        {
            string misses = "";

            if (Port <= 0) misses += " port,";
            if (Mps <= 0) misses += " mps,";
            if (Hostname == null || Hostname.Length == 0) misses += " hostname";

            if (misses.Length > 0)
            {
                ErrorMessage = "Missing attributes:" + misses;
                IsFaulted = true;
                return false;
            }

            if (Size < 50 || Size > 3000)
            {
                ErrorMessage = "Size must be 50 at least and 3000 at most.";
                IsFaulted = true;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Start TCP communication with the client defined by <see cref="Hostname"/>, asynchronously.
        /// Message format is according to <see cref="Message.Bytes"/>.
        /// Runs infinitely until told to stop (<see cref="AskStop"/>).
        /// </summary>
        /// <returns></returns>
        
        public async Task StartAsync()
        {
            try
            {
                if (!CheckConfig()) return;

                _stop = false;
                _generatingStatistics = false;
                _takingResult = false;
                _messagesSentOverall = 0;
                
                _client = new TcpClient(Hostname, Port); // Attempt connection.
                _client.SendBufferSize = 3000;
                _client.ReceiveBufferSize = 3000;

                _results = new List<PingResult>();

                var swPeriod = new Stopwatch(); // Secure operation is repeated according to Mps
                swPeriod.Start();

                while (!_stop && _mainThread.IsAlive && !IsFaulted)
                {
                    // Wait to send message according to Mps
                    while (swPeriod.ElapsedTicks < PeriodSwTicks)
                    {
                        if (_stop || !_mainThread.IsAlive || IsFaulted) return;
                    }

                    swPeriod.Restart();

                    // Ping asynchronously. It will never take more than 1 / Mps.
                    var task = new Task<PingResult>(Ping);
                    task.Start();
                    var result = await task; // Receive PingResult from async request.

                    // _result must not be tampered with if statistics are being fetched, so wait here.
                    while (_generatingStatistics)
                    {
                        if (_stop || !_mainThread.IsAlive || IsFaulted) return;
                    }
                    
                    // Statictics can not be taken while _result is being updated, so make it wait.
                    _takingResult = true;
                    _results.Add(result);
                    _takingResult = false;
                }
            }
            catch (Exception ex)
            {
                IsFaulted = true;
                ErrorMessage = ex.Message + "\n" + ex.StackTrace;
            }
        }

        /// <summary>
        /// Ping the Catcher. Note all send/receive times. This will never last more than <see cref="PeriodTs"/>.
        /// </summary>
        /// <returns><see cref="PingResult"/> instance containing noted send/receive times and <see cref="PingResult.failed"/> is true, 
        /// or in case of failure a <see cref="PingResult"/> instance where <see cref="PingResult.failed"/> is true</returns>

        private PingResult Ping()
        {
            try
            {
                lock (_client)
                {
                    Message request = new Message(Size); // Generate message.
                    Message response = null;
                    _messagesSentOverall++;
                    _currentMessage = request; // Makes sure failed messaged number is correct.

                    NetworkStream stream = _client.GetStream();
                    byte[] bytes = request.Bytes; // buffer for send/receive action.

                    // Makes sure ping does not last more than period time. 
                    // If it does, failed PingResult is returned.
                    var timeout = PeriodTs;
                    bool success = false;

                    var startTime = DateTime.UtcNow;
                    request.AssumeSendNow();

                    success = stream.FlushAsync().Wait(timeout);
                    if (!success) return FailedResultForCurrent;

                    timeout = PeriodTs - (DateTime.UtcNow - startTime);
                    if (timeout.Ticks < 0) return FailedResultForCurrent;

                    success = stream.WriteAsync(request.Bytes, 0, bytes.Length).Wait(timeout);
                    if (!success) return FailedResultForCurrent;

                    timeout = PeriodTs - (DateTime.UtcNow - startTime);
                    if (timeout.Ticks < 0) return FailedResultForCurrent;

                    success = stream.ReadAsync(bytes, 0, Size).Wait(timeout);
                    if (!success) return FailedResultForCurrent;

                    var rtt = DateTime.UtcNow - startTime; // Round Trip Time, total time.

                    var endTime = DateTime.UtcNow;
                    var endMicros = endTime.Ticks / 10;
                    response = new Message(bytes); // Generate response message.

                    var result = new PingResult
                    {
                        messageNumber = _currentMessage.Number,
                        failed = false,
                        receiveMillis = (int)(endMicros - response.SentMicrosecUtc) / 1000,
                        sendMillis = (int)(response.SentMicrosecUtc - request.SentMicrosecUtc) / 1000,
                        roundMillis = (int)rtt.TotalMilliseconds
                    };

                    return result;
                }
            }
            catch (Exception ex)
            {
                IsFaulted = true;
                ErrorMessage = ex.Message + "\n" + ex.StackTrace;
                return FailedResultForCurrent;
            }
        }

        /// <summary>
        /// Get statictics since last result reset.
        /// </summary>
        /// <param name="reset">Should result list be cleared after generating statistics.</param>
        /// <returns>Statistics since last reset.</returns>

        public Statistics GetStatistics(bool reset)
        {
            // Makes sure result noting waits until operation is done.
            // This way, method can be called from any thread.
            _generatingStatistics = true;

            // But if result taking already started, wait for it to finish.
            while (_takingResult) { }

            lock (_results)
            {
                var successes = _results.Count(r => !r.failed);

                Statistics statictics = null;

                if (successes == 0)
                {
                    statictics = new Statistics
                    {
                        MessagesOverall = _messagesSentOverall,
                        MessagesLastReset = 0,
                        Taken = DateTime.Now,

                        FailedMessageNumbers = _results.Where(r => r.failed).Select(r => r.messageNumber).ToList(),
                        AvgReceiveMs = -1,
                        AvgSendMs = -1,
                        AvgRoundMs = -1,

                        MaxReceiveMs = -1,
                        MaxSendMs = -1,
                        MaxRoundMs = -1
                    };
                }
                else
                {
                    statictics = new Statistics
                    {
                        MessagesOverall = _messagesSentOverall,
                        MessagesLastReset = _results.Count,
                        Taken = DateTime.Now,

                        FailedMessageNumbers = _results.Where(r => r.failed).Select(r => r.messageNumber).ToList(),

                        AvgReceiveMs = _results.Where(r => !r.failed).Aggregate(0, (sum, next) => sum += next.receiveMillis) / successes,
                        AvgSendMs = _results.Where(r => !r.failed).Aggregate(0, (sum, next) => sum += next.sendMillis) / successes,
                        AvgRoundMs = _results.Where(r => !r.failed).Aggregate(0, (sum, next) => sum += next.roundMillis) / successes,

                        MaxReceiveMs = _results.Where(r => !r.failed).Max(r => r.receiveMillis),
                        MaxSendMs = _results.Where(r => !r.failed).Max(r => r.sendMillis),
                        MaxRoundMs = _results.Where(r => !r.failed).Max(r => r.roundMillis)
                    };
                }

                if (reset)
                {
                    _results.Clear();
                }

                _generatingStatistics = false;

                return statictics;
            }
        }

        /// <summary>
        /// Makes <see cref="StartAsync"/> stop when it is ready to.
        /// </summary>

        public void AskStop()
        {
            _stop = true;
        }
    }
}
