﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PingZadatak.Engine
{
    /// <summary>
    /// Generates response for a ping request structures according to <see cref="Message.Bytes"/>.
    /// </summary>
    /// 
    public class Catcher
    {
        public int Port { get; set; }
        public string Bind { get; set; }

        public string ErrorMessage { get; set; }
        public bool IsFaulted { get; set; }

        public List<int> AcceptedMessageNumbers { get; set; }
        
        private TcpListener _listener;
        private TcpClient _client;

        private Thread _mainThread;

        private bool _stop;

        public Catcher(Thread mainThread)
        {
            _mainThread = mainThread;
            IsFaulted = false;
            AcceptedMessageNumbers = new List<int>();
        }

        /// <summary>
        /// If configuration is invalid, sets <see cref="IsFaulted"/> to true and sets <see cref="ErrorMessage"/>.
        /// </summary>
        /// <returns>If configuration is correct.</returns>

        private bool CheckConfiguration()
        {
            string misses = "";

            if (Port <= 0) misses += " port,";
            if (Bind == null || Bind.Length == 0) misses += " bind,";

            if (misses.Length > 0)
            {
                ErrorMessage = "Missing attributes:" + misses;
                IsFaulted = true;
                return false;
            }
            
            IPAddress ip = null;
            var ipvalid = IPAddress.TryParse(Bind, out ip);

            if (!ipvalid)
            {
                ErrorMessage = "Invalid bound IP address (bind attribute)";
                IsFaulted = true;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Start listening for ping requests and generating responses to them, asynchronously.
        /// Message format is according to <see cref="Message.Bytes"/>.
        /// Runs infinitely until told to stop (<see cref="AskStop"/>).
        /// </summary>
        /// <returns></returns>

        public async Task StartAsync()
        {
            try
            {
                if (!CheckConfiguration())
                    return;

                AcceptedMessageNumbers = new List<int>();

                _stop = false;

                _listener = new TcpListener(IPAddress.Any, Port);
                _listener.Start();

                Console.WriteLine("\nWaiting for connection...");

                _client = _listener.AcceptTcpClient();

                Console.WriteLine("Connected to client!");

                var task = new Task(Communicate);
                task.Start();
                await task;

                _listener.Stop();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                IsFaulted = true;

                if (_listener != null)
                    _listener.Stop();
            }
        }

        /// <summary>
        /// Listen for Ping requests, send back response containing its own sent time. 
        /// Message format is according to <see cref="Message"/>.
        /// Runs infinitely until told to stop (<see cref="AskStop"/>).
        /// </summary>

        private void Communicate()
        {
            NetworkStream stream = _client.GetStream();

            var byteBuffer = new byte[3000];
            int length = 3000;
            

            while (!_stop && !IsFaulted && _mainThread.IsAlive)
            {
                // Receive message bytes.

                int read = stream.Read(byteBuffer, 0, length);

                if (_stop || IsFaulted || !_mainThread.IsAlive)
                    break;

                if (read == 0)
                    continue;

                length = read;

                var bytes = new byte[read];
                Buffer.BlockCopy(byteBuffer, 0, bytes, 0, read);

                var request = new Message(bytes); // Generate request message from bytes.

                AcceptedMessageNumbers.Add(request.Number);

                var response = new Message(request.Length, request.Number); // Generate response message.

                stream.Flush();
                stream.Write(response.Bytes, 0, response.Length);
            }
        }

        /// <summary>
        /// Makes <see cref="StartAsync"/> stop when it is ready to.
        /// </summary>

        public void AskStop()
        {
            _stop = true;
        }
    }
}
