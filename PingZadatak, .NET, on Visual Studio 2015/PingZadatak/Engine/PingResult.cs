﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PingZadatak.Engine
{
    public class PingResult
    {
        public int messageNumber;
        public bool failed;

        public int sendMillis;
        public int receiveMillis;
        public int roundMillis;

        public override string ToString()
        {
            return $"{ messageNumber }\t{ failed }" +
                   $"\tSending: { sendMillis }\tReceiving: { receiveMillis }\tRound: { roundMillis }";
        }
    }
}
