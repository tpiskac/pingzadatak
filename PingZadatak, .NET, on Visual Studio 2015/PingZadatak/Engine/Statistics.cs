﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PingZadatak.Engine
{
    public class Statistics
    {
        public DateTime Taken { get; set; }
        public int MessagesOverall { get; set; }
        public int MessagesLastReset { get; set; }

        public int AvgSendMs { get; set; }
        public int AvgReceiveMs { get; set; }
        public int AvgRoundMs { get; set; }

        public int MaxSendMs { get; set; }
        public int MaxReceiveMs { get; set; }
        public int MaxRoundMs { get; set; }

        public List<int> FailedMessageNumbers { get; set; }

        public override string ToString()
        {
            var lost = FailedMessageNumbers.Aggregate("", (acc, n) => acc += n + ", ");

            return $"{ Taken.ToString("HH:mm:ss") }\tOverall: { MessagesOverall }\t\tLast second: { MessagesLastReset }" +
                   $"\tAvg RTT: { AvgRoundMs }ms\nAvg send: { AvgSendMs }ms\tAvg receive: { AvgReceiveMs }ms" +
                   $"\tMax RTT: { MaxRoundMs }ms\tMax send: { MaxSendMs }ms\tMax receive: { MaxReceiveMs }ms" +
                   (FailedMessageNumbers.Count > 0 ? $"\nLost messages: { lost }" : "");
        }
    }
}
